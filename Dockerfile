FROM buildkite/puppeteer:latest
WORKDIR /app
COPY ./package.json ./
RUN npm install
COPY src src
COPY nest-cli.json nest-cli.json
COPY nodemon.json nodemon.json
COPY tsconfig.json tsconfig.json
ENTRYPOINT ["npm", "run", "start:dev"]