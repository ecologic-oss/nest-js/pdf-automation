FROM buildkite/puppeteer:latest
WORKDIR /app
COPY ./package.json ./
RUN npm install
COPY src src
COPY tsconfig.json tsconfig.json
COPY tsconfig.build.json tsconfig.build.json
COPY nest-cli.json nest-cli.json
ENV NODE_ENV=staging
ENTRYPOINT ["npm", "run", "start:prod"]