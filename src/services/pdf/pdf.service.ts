import {Injectable} from '@nestjs/common';
import * as puppeteer from 'puppeteer';
import {Duplex} from 'stream';
import {PdfCreationDto} from 'src/controllers/pdf/pdf-creation.dto';

@Injectable()
export class PdfService {
    public async createPdf(parameters: PdfCreationDto): Promise<Duplex> {
        const browser = await puppeteer.launch({args: ['--no-sandbox', '--disable-setuid-sandbox']}); // --no-sandbox should not be needed after correctly dockerizing this app --> puppeteer.launch({ headless: true }); is enought then
        const page = await browser.newPage();

        if (parameters.token) {
            console.log(`Using url: ${parameters.tokenSetUrl} to set token ${parameters.token.substring(0, 5)}...`);
            await page.goto(parameters.tokenSetUrl, {waitUntil: 'load'});
            await page.evaluate((token: string) => {
                localStorage.setItem('access_token', token);
            }, parameters.token);
        }

        console.log(`Creating Pdf using ${parameters.url}`);
        await page.goto(parameters.url, {waitUntil: 'domcontentloaded'});
        await page.waitForSelector('#content-loaded');

        const pdfBuffer = await page.pdf({
            format: 'A4',
            printBackground: true,
            displayHeaderFooter: true,
            headerTemplate: parameters.headerTemplate,
            footerTemplate: parameters.footerTemplate,
            margin: {
                top: parameters.headerHeight,
                bottom: parameters.footerHeight,
            },
        });

        await browser.close();
        return PdfService.bufferToStream(pdfBuffer);
    }

    private static bufferToStream(buffer: Buffer): Duplex {
        const stream = new Duplex();
        stream.push(buffer);
        stream.push(null);
        return stream;
    }
}
