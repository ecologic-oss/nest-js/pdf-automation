import {Module} from '@nestjs/common';
import {PdfController} from './controllers/pdf/pdf.controller';
import {HealthController} from './controllers/health/health.controller';
import {PdfService} from './services/pdf/pdf.service';

@Module({
    controllers: [PdfController, HealthController],
    providers: [PdfService]
})
export class AppModule {
}