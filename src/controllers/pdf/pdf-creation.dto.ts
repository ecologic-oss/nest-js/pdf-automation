export class PdfCreationDto {
  token: string;
  tokenSetUrl: string;
  url: string;
  filename: string;
  headerTemplate: string;
  headerHeight: string;
  footerTemplate: string;
  footerHeight: string;
}
