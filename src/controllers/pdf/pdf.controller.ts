import {Controller, Post, Header, Res, Body} from '@nestjs/common';
import {PdfService} from '../../services/pdf/pdf.service';
import {PdfCreationDto} from './pdf-creation.dto';

@Controller('pdf')
export class PdfController {
    constructor(private readonly pdfService: PdfService) {
    }

    @Post('file')
    @Header('content-type', 'application/pdf')
    async file(@Res() res, @Body() dto: PdfCreationDto) {
        const filename = dto.filename || 'download.pdf';
        res.set('Content-Disposition', `attachment; filename=${filename}`);
        (await this.pdfService.createPdf(dto)).pipe(res);
    }

    @Post('blob')
    @Header('content-type', 'application/octet-stream')
    async blob(@Res() res, @Body() dto: PdfCreationDto) {
        (await this.pdfService.createPdf(dto)).pipe(res);
    }
}
