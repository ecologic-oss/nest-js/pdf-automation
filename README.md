## Description

[NestJs](https://github.com/nestjs/nest) application using [pupeteer](https://github.com/GoogleChrome/puppeteer) to generate the unicef report pdf.

## Development

```bash
# install deps
npm install

# run the app
npm run start:dev

# health-endpoint should now be up
curl localhost:3000/health # response: {"status":"ok"}
```

## Create the PDF

Use your favorite rest-client targeting: http://localhost:3000/pdf/blob (or /file) with body:

```
{
  "token": "<your token>",
  "tokenSetUrl": "http://localhost:4200/token",
	"url": "http://localhost:4200/pdf-preview",
	"headerTemplate": "<span style='font-size: 9px; width: 100vw; text-align: center; color: black; margin: 0.2cm 0; font-family: Verdana, Arial, sans-serif;'>Test Header</span>",
	"footerTemplate": "<span style='font-size: 9px; width: 100vw; text-align: center; color: black; margin: 0.2cm 0; font-family: Verdana, Arial, sans-serif;'>Test Footer</span>"
}
```

`token` and `tokenSetUrl` are optional.

## Requirements on the endpoint

There needs to be a HTML Element with the id `content-loaded` present on the rendered HTML.
